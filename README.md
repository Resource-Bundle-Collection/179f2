# Cygwin安装与apt-cyg配置完全指南

欢迎来到Cygwin安装及apt-cyg工具使用的详细教程。本教程将引导您完成在Windows系统上安装Cygwin的步骤，配置并有效利用apt-cyg来管理您的Cygwin软件包，同时解决可能出现的特定错误。如果您遇到“/usr/bin/apt-cyg: line 25: $'\r': command not found”的错误，我们也将提供解决方案，确保您的Cygwin环境顺利运行。

## Cygwin简介
Cygwin是一款让Windows操作系统能够运行Linux应用程序的神奇工具集。它通过提供一个兼容层，让您能在Windows环境下体验Unix/Linux风格的环境和工具。

## 安装Cygwin
1. **下载安装程序**：首先，从[Cygwin官方网站](http://cygwin.com/)下载最新的安装程序。
2. **启动安装**：双击下载好的安装包，按照向导进行。
3. **选择站点**：推荐选择国内镜像以加快下载速度，如阿里云等。
4. **自定义安装**：关键步骤是，在软件选择界面，切换到Full视图，并搜索添加`wget`工具，这对后期使用apt-cyg至关重要。
5. **完成安装**：持续下一步直至安装完成，根据个人喜好可选择是否创建桌面快捷方式。

## 配置apt-cyg
apt-cyg是一个类似于Debian系统的apt-get的软件包管理工具，极大地简化了Cygwin中软件包的查找与安装。
1. **手动创建apt-cyg脚本**：打开文本编辑器，如Notepad，将提供的脚本代码复制至新建文档，并保存为`apt-cyg`，确保保存时文件扩展名为`.bash`或无扩展名，放置于Cygwin的`bin`目录下。
   
   **注意**：请确保脚本中不含有导致错误的换行符（如Windows系统的`\r\n`）。

2. **权限设置**：确保此脚本具有执行权限，通常Cygwin安装会自动处理这一点。

## 使用apt-cyg
安装完成后，通过Cygwin终端输入`apt-cyg`命令即可开始使用。您可以使用它来进行软件包的安装、删除、查询等多种操作。

### 解决常见错误
如果遇到“/usr/bin/apt-cyg: line 25: $'\r': command not found”错误，通常是因为脚本包含不正确的换行符。请检查并替换为正确的LF（Unix风格）换行符。

### 示例命令
- 安装软件包：`apt-cyg install 包名`
- 删除软件包：`apt-cyg remove 包名`
- 更新软件列表：`apt-cyg update`

通过遵循上述步骤，您将能够有效地管理和扩展您的Cygwin环境，享受类似Linux的开发和管理体验。